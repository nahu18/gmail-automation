package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class GmailAutomation {




        public static void main(String[] args) throws InterruptedException, IOException {

            System.setProperty("webdriver.chrome.driver", "C:\\Gecko\\chromedriver.exe");
            WebDriver driver;
            ChromeOptions chromeOptions= new ChromeOptions();
            driver=new ChromeDriver(chromeOptions);;
            chromeOptions.addArguments("--headless");
            chromeOptions.addArguments("--no-sandbox");

            String url="https://mail.google.com/";

            driver.get(url);
            String username= "enter email";
            String password= "enter password";

            driver.findElement(By.xpath("//*[@id=\"identifierId\"]")).clear();
            driver.findElement(By.xpath("//*[@id=\"identifierId\"]")).sendKeys(username, Keys.ENTER);

            Thread.sleep(10000);
            driver.findElement(By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input")).sendKeys(password, Keys.ENTER);

            List<WebElement> Inbox = driver.findElements(By.xpath("//*[@class='zA zE']"));

            for(int i = 0; i < Inbox.size(); i++) {
                BufferedWriter writer = new BufferedWriter(new FileWriter("Unread Emails.txt", true));
                writer.write(Inbox.get(i).findElement(By.xpath(".//*[@class='bog']")).getText());
                writer.close();
            }
            Thread.sleep(2000);

            driver.quit();
        }
    }



